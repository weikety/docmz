<?php

namespace Admin\Controller;

class CmsDocCatController extends CmsController
{
    // 导出菜单
    // cmslist、cmshandle为必须要到导出的字段
    static $export_menu = array(
        'content' => array(
            '文档分类' => array(
                'cmslist' => array(
                    'title' => '文档分类',
                    'hiddens' => array(
                        'cmshandle' => '文档分类管理'
                    )
                )
            )
        )
    );
    // 标识字段，该字段为自增长字段
    public $cms_pk = 'id';
    // 数据表名称
    public $cms_table = 'cms_doc_cat';
    // 数据库引擎
    public $cms_db_engine = 'MyISAM';
    // 列表列出的列出字段
    public $cms_fields_list = array(
        'id',
        'uid',
        'title',
    );
    // 添加字段，留空表示所有字节均为添加项
    public $cms_fields_add = array();
    // 编辑字段，留空表示所有字节均为编辑项
    public $cms_fields_edit = array();
    // 搜索字段，表示列表搜索字段
    public $cms_fields_search = array();
    // 数据表字段
    public $cms_fields = array(

        'uid' => array(
            'title' => '用户ID',
            'description' => '',
            'type' => 'member_uid',
            'default' => '0',
            'rules' => 'required|searchable'
        ),

        'doc_cnt' => array(
            'title' => '文档数',
            'description' => '',
            'type' => 'number',
            'default' => '0',
            'rules' => ''
        ),

        'title' => array(
            'title' => '分类名称',
            'description' => '',
            'type' => 'text',
            'length' => 10,
            'default' => '',
            'rules' => ''
        ),

    );
}
