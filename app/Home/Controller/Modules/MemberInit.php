<?php
/*
 * 用户自动登录并检测
 *
 * 自动设定宏定义
 *      MEMBER_LOGINED(boolean)
 *      MEMBER_LOGINED_UID(number)
 *
 * 自动设定模板变量
 *      data_email
 *      data_username
 *      data_avatar_url_b
 *      data_avatar_url_m
 *      data_avatar_url_s
 *      data_nickname(username || email)
 *
 */
if (!defined('THINK_PATH')) {
    exit();
}
$this->_am = D('Member', 'Service');
if ($user_uid = I('session.member_user_uid', 0, 'intval')) {
    define ('MEMBER_LOGINED', $this->_am->init($user_uid));
    if (MEMBER_LOGINED) {
        define ('MEMBER_LOGINED_UID', $this->_am->get('user.uid'));
        $this->data_email = $this->_am->get('user.email');
        $this->data_username = $this->_am->get('user.username');
        $this->data_avatar_url_b = $this->_am->get_avatar_url(-1, 'b');
        $this->data_avatar_url_m = $this->_am->get_avatar_url(-1, 'm');
        $this->data_avatar_url_s = $this->_am->get_avatar_url(-1, 's');
        if ($username = $this->_am->get('user.username')) {
            $this->data_nickname = $username;
        } else {
            $email = $this->_am->get('user.email');
            $this->data_nickname = substr($email, 0, strpos($email, '@'));
        }
    } else {
        unset($_SESSION['member_user_uid']);
        if (CONTROLLER_NAME == 'User' && in_array(ACTION_NAME, array(
                'register',
                'login',
                'forgetpwd'
            ))
        ) {
            // EMPTY
        } else {
            unset ($_SESSION ['login_user_id']);
            $this->redirect(U('User/login'));
        }
    }
} else {
    define ('MEMBER_LOGINED', false);
}
if (!defined('MEMBER_LOGINED_UID')) {
    define ('MEMBER_LOGINED_UID', 0);
}