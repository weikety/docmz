<?php
/*
 * 退出登录
 * 用于在 /User/logout 页面
 *
 * 需要预先设定的值
 *      $redirect [可选]
 */
if (!defined('THINK_PATH')) {
    exit();
}
if (empty($redirect)) {
    $redirect = __ROOT__ . '/';
}

unset ($_SESSION ['member_user_uid']);
$this->redirect($redirect);